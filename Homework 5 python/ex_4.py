#Write a Python program to find the second smallest number in a list.
print("Ex.4 Second Smallest")

arr = [25, 1,5, 13]
small = arr[0]

for i in arr:
    if small > i:
        small = i

arr2 = []
for i in arr:
    if i != small :
        arr2.append(i)

small = arr2[0]
for i in arr2:
    if small > i:
        small = i

print("The second smallest is: " + str(small))