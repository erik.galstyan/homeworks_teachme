# Append new string in the middle of a
# given(even number of characters) string
# Example:
# Sample = ‘python’
# new_string = ‘new’
# Expected ‘pytnewhon

print("Exercise 9")
sample = input("Sample: ")
new_string = input("new_string: ")
size = len(sample)


if size % 2 == 0 :
    mid = size//2
    expected = sample[:mid] + new_string + sample[mid:] 
    print(expected)