# Write a Python program to change a given string to a new string where the first and last chars
# have been exchanged.
# Example:
# Sample: ‘abcdefgh’
# Expected: ‘hbcdefga’
print("Exercise 4")
str1 = input("Enter your string: ")
first = str1[0]
end = str1[-1]
new = end + str1[1:-1] + first
print(new)