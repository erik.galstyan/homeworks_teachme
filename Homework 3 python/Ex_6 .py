# Write a Python function to get a string made of 
# its first three characters of a specified string. If
# the length of the string is less than 3 then return the original string.
# e.g.
# Sample ='ipy'
# Expected ipy
# ______________
# Sample ='python'
# Expected pyt

print("Exercise 6")
str1 = input("Enter your string: ")
size = len(str1)

if size < 3 :
    print(str1)
else :
    print(str1[:3])
