# Write a Python function to get a string 
# made of 4 copies of the last two characters of a
# specified string (length must be at least 2).
# e.g.
# Sample = ‘Python'
# Expected onononon
# ________________
# Sample 'Exercises'
# Expected eseseses
print("Exercise 5")
str1 = input("Enter your string: ")
last = str1[-2:]
new = 4 * last
print(new)