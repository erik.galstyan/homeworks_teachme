# Write a function in Python to count
# uppercase characters in a text file.

def uppers(x):
    file = open(x,"r")
    reading = file.read()
    all_lines = reading.split("\n")
    bigs = 0
    for line in all_lines:
        for word in line:
            if word.isupper():
                bigs += 1
    file.close()

    return bigs

func = uppers("story.txt")
print(func)
