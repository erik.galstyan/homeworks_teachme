# Write a function in Python to count
# words in a text file those are
# ending with letter "e"

def ends_e(x):
    file = open(x,"r")
    reading = file.read()
    all_lines = reading.split("\n")
    count = 0
    for line in all_lines:
        words = line.split()
        for i in words:
            if i[-1]=="e":
                count += 1
                print(i)
    file.close()

    return count


func = ends_e("note.txt")
print(func)