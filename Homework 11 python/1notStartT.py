# Write a Python function to count the 
# number of lines from a text file
# "story.txt" which is not starting 
# with a letter "T".

def not_start_t(x):
    file = open(x,"r")
    reading = file.read()
    lines = reading.split("\n")
    count = 0
    for x in lines:
        if x[0] != "T":
            count += 1
    file.close()
    
    return count
        

func = not_start_t("story.txt")
print(func)