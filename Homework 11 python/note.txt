These are the powerful attributes of the S-Class.
The first luxury saloon to bear this name made 
its debut 50 years ago: a new star was born! 
The abbreviation stands for "special class",
but could just as well symbolise "style" or "star".
Today, the Mercedes-Benz 116 series inspires
people just as much as it did in 1972.
[1, 4, 9, 16, 25, 36, 49, 64]