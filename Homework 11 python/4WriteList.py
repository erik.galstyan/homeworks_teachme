#Write a Python function to write a list to a file

def write_list(x):
    your_list = [1,4,9,16,25,36,49,64]

    file = open(x,"a")
    file.write(f"\n{your_list}")
    file.close()

    file = open(x,"r")
    print(file.read())
    file.close()


write_list("note.txt")