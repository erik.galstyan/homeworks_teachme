# Write a Python program to convert a datetime.now() 
# like output format.
# Sample String : datetime.datetime.now()
# Expected Output : 2014-07-01 14:43:00
import datetime as dt
from time import strftime

date = dt.datetime.now()
year = date.strftime("%Y")
month = date.strftime("%m")
day = date.strftime("%d")
today = f"{year}-{month}-{day}"
print(today,date.strftime("%X"))

#karch grel nuyny google-ic nayelov ->
esor = date.strftime("%Y-%m-%d")
print(esor,strftime("%X"))
