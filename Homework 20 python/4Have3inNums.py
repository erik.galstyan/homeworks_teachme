#Find all of the numbers from 1-1000 that have a 3 in them

def get_digit_3(num):
    str_num = str(num)
    for y in str_num:
        if y == '3':
            return True

    return False


nums = [x for x in range(1,1000) if get_digit_3(x)]
#nums = [x for x in range(1,1000) if '3' in str(x)] best variant
print(nums)
