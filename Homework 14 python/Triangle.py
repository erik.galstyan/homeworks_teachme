from math import sqrt

class Triangle:
    right_angled = True

    def __init__(self,a,b,c):
        self.a = a
        self.b = b
        self.c = c

    def get_perimetr(self):
        return self.a + self.b + self.c

    def get_area(self):
        a = self.a
        b = self.b
        c = self.c
        P =(a+b+c)/2
        s = sqrt(P*(P-a)*(P-b)*(P-c))
        return s

a = int(input("Enter a: "))
b = int(input("Enter b: "))
c = int(input("Enter c: "))
triangle = Triangle(a,b,c)
print('The perimetr: ',triangle.get_perimetr())
print('The area: ',triangle.get_area())



