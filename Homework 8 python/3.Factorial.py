# Write a Python function to calculate the factorial of a number
# (a non-negative integer). The function accepts the number as an argument.
def factorial(x):
    i = 1
    num = x
    while 1 <= x:
        i *= x
        x -= 1

    print(f"The factorial of {num} is {i}")


num = int(input("Enter num: "))
factorial(num)