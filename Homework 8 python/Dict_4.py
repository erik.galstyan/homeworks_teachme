#Write a python function which create dict from 2
#lists with the same length

def create(list_1,list_2):
    dnew = {}
    for i in range(len(list_1)):
        dnew[list_1[i]] = list_2[i]

    return dnew
        


l_1 = ["a","b","c","d","e"]
l_2 = [10,20,30,15,25]

if len(l_1) == len(l_2):
    print(create(l_1,l_2))
else:
    print("You can't create dict")