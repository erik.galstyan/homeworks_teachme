#Write a python function, which add a new value
#with given key in dict.

def add_value(dic,key,value):
    dic[key] = value


d = { 1:500, 2:1000, 3:2000, 4:5000}
key = input("Enter key: ")
value = input("Enter value: ")
add_value(d,key,value)
print(d)