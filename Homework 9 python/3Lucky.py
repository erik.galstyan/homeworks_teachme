# Ticket numbers usually consist of an even 
# number of digits. A ticket number is
# considered lucky if the sum of the first 
# half of the digits is equal to the sum of the
# second half.
# Given a ticket number n, determine if it's lucky or not.

def is_lucky(x):
    str1 = str(x)
    size = len(str1)
    first = str1[:size//2]
    second = str1[size//2:]

    sum1 = 0
    for i in first:
        sum1 += int(i)
    sum2 = 0
    for i in second:
        sum2 += int(i)

    if sum1 == sum2:
        return True
    else:
        return False
    
x = int(input("Enter num: "))
if is_lucky(x):
     print(x," Is Lucky")
else:
     print("Try again!")