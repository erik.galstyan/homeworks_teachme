# Write a function that checks if a given number is Palindrome or not.
# A palindrome is a word, number, phrase, 
# or other sequence of characters which reads the
# same backward as forward, such as madam or racecar.

def check(x):
    num = str(x)
    size = len(num)
    for i in range(size):
        palind = False
        if num[i] == num[size-1-i]:
            palind = True
 
    return palind


num = input("Enter word: ")
print(check(num))