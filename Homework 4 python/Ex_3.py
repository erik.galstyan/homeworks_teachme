# Input a two-digit natural number and output the sum of its digits. You can
# assume that the input will be a two-digit number and need not check that
# programmatically.
print("Exercise 3")

x = int(input("Enter two-digit number: "))
sum = x//10 + x%10
print(sum)