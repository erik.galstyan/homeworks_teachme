# The program prompts the user their birth year. Assuming a person’s age
# is a non-negative integer not exceeding 120, output the user’s age or the
# words “Incorrect Year”. The sample outputs assume it’s currently the year
# 2016. If you are writing this program during any other year, the correct
# answers may differ. Store the value of the current year in a variable.
print("Exercise 7")

year = int(input("Enter your birth year: "))
x = 2016 - year

if x >= 0 :
    if x <= 120:
     print(x)
    else:
     print("Incorrect year")
else:
    print("Incorrect year")