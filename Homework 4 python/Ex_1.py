# Input two integers and print out their sum. Preserve the exact format from
# the examples (e.g. the output should contain exactly three lines)
print("Exercise 1")

a = int(input("Enter a: "))
b = int(input("Enter b: "))
print("a = ",a)
print("b = ",b)
print("a + b = ",a , " + ",b," = ",a+b)
