# Write a Python program to square and
#  cube every number in a given list of
# integers using Lambda.

# # Sample. ([1,2,3,4,5])
# # Output: ([1, 4, 9, 16, 25], [1, 8, 27, 64, 125])

sample = [1,2,3,4,5]

square = lambda x: x**2
cube = lambda x: x**3

sq_list = list(map(square,sample))
cub_list = list(map(cube,sample))

out = (sq_list,cub_list)
print(out)

