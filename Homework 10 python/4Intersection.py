# Write a Python program to find intersection 
# of two given arrays using Lambda.

# Original arrays:
# [1, 2, 3, 5, 7, 8, 9, 10]
# [1, 2, 4, 8, 9]

arr1 = [1, 2, 3, 5, 7, 8, 9, 10]
arr2 = [1, 2, 4, 8, 9]

result = list(filter(lambda x: x in arr1,arr2))
print("Intersection of the said arrays:",result)